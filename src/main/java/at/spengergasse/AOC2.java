package at.spengergasse;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;

public class AOC2 {

    private List<String> input;

    public void run() throws IOException {
        Path filePath = new File("input.txt").toPath();
        Charset charset = Charset.defaultCharset();
        this.input = Files.readAllLines(filePath, charset);

        long startOne = System.currentTimeMillis();
        int resultOne = part1();
        long startTwo = System.currentTimeMillis();
        String resultTwo = part2();

        System.out.println("PART1: " + resultOne + " - Took: " + (System.currentTimeMillis() - startOne) + " ms");
        System.out.println("PART2: " + resultTwo + " - Took: " + (System.currentTimeMillis() - startTwo) + " ms");
    }


    public int part1() {
        int countTwo = 0, countThree = 0;

        for (String s: this.input) {
            HashMap<Character, Integer> charMap = new HashMap<>();
            for (Character c: s.toCharArray()) {
                if (charMap.containsKey(c)) {
                    charMap.put(c, charMap.get(c) +1);
                } else {
                    charMap.put(c, 1);
                }
            }

            boolean hasTwo = false, hasThree = false;
            for (Character c: charMap.keySet()) {
                if (charMap.get(c) == 2) {
                    hasTwo = true;
                } else if (charMap.get(c) == 3) {
                    hasThree = true;
                }
                if (hasTwo && hasThree) {
                    break;
                }
            }

            if (hasTwo)
                countTwo++;

            if (hasThree)
                countThree++;
        }
        return countTwo * countThree;
    }

    public String part2() {
        for (int i = 0; i < this.input.size(); i++) {
            String s1 = this.input.get(i);
            for (int j = i + 1; j < this.input.size(); j++) {
                String s2 = this.input.get(j);
                int count = 0, pos = 0;

                for (int g = 0; g < s1.length(); g++) {
                    if (s1.charAt(g) != s2.charAt(g)) {
                        if (count++ > 1) break;
                        pos = g;
                    }
                }
                if (count == 1) {
                    StringBuilder sb = new StringBuilder(s1);
                    sb.deleteCharAt(pos);
                    return sb.toString();
                }
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        new AOC2().run();
    }

}
